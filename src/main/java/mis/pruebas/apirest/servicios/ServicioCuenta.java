package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {
    public List<Cuenta> obtenerTodosCuentas();
    //CREATE
    public void insertarCuentaNuevo(Cuenta Cuenta);
    //READ
    public Cuenta obtenerCuenta(String documento);
    //UPDATE
    public void guardarCuenta(Cuenta Cuenta);
    public void emparcharCuenta(Cuenta parche);
    //delete
    public void borrarCuentas(String documento);
}
