package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    @GetMapping
    public List<Cuenta> obtenerCuenta() {
        return this.servicioCuenta.obtenerTodosCuentas();
    }
    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta Cuenta) {
        this.servicioCuenta.insertarCuentaNuevo(Cuenta);
    }
    @GetMapping("/{numero}")
    public  Cuenta obtenerUnCuenta(@PathVariable String numero) {
        return  this.servicioCuenta.obtenerCuenta(numero);
    }
    @PutMapping("/{numero}")
    public void reemplazarUnCuenta(@PathVariable("numero") String nroNumero,@RequestBody Cuenta Cuenta) {
        Cuenta.numero=nroNumero;
        this.servicioCuenta.guardarCuenta(Cuenta);
    }

    @PatchMapping("/{numero}")
    public void emparacharUnCuenta(@PathVariable("numero") String nroNumero,@RequestBody Cuenta Cuenta) {
        Cuenta.numero=nroNumero;
        this.servicioCuenta.emparcharCuenta(Cuenta);
    }

    @DeleteMapping("/{numero}")
    public void eliminarCuenta(@PathVariable("numero") String nroNumero) {

        this.servicioCuenta.borrarCuentas(nroNumero);
    }

}
